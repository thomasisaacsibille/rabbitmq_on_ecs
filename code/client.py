import os
import time
import json
import threading

from kombu import Connection, Exchange, Producer, Queue


with open('env.json') as json_file:
    data = json.load(json_file)
    rabbit_url = "amqp://rabbit:%s@%s:5672/" % (data['rabbit_password']['value'],data['rabbitmq_elb_dns']['value'])


msg_sent = 0
msg_process = 0
connection_reset = 0

SLEEP_TIME=0.1


def get_producer():
    exchange, channel = establish_connection()
    return Producer(exchange=exchange, channel=channel, routing_key="BOB")

def establish_connection():

    conn = Connection(rabbit_url)
    channel = conn.channel()
    exchange = Exchange("exchange1", type="direct")
    queue = Queue(name="queue1", exchange=exchange, routing_key="BOB")
    queue.maybe_bind(conn)
    queue.declare()

    return exchange, channel

    


def send_messages():
    # producer = get_producer()
    global msg_sent
    while True:
        # producer.publish(time.time())
        msg_sent =  msg_sent + 1
        time.sleep(SLEEP_TIME)



def process_messages():
    # producer = get_producer()
    global msg_process
    while True:

        msg_process = msg_process + 1
        # producer.publish(time.time())
        time.sleep(SLEEP_TIME)



def display(event):
    while not event.is_set():
        print("Connection reset: %d" % connection_reset)
        print("Messages sent: %d" % msg_sent)
        print("Messages process: %d" % msg_process)
        time.sleep(0.5)   

def publish(event):
    while True:
        send_messages()
        time.sleep(1)   

def read(event):
    while True:
        process_messages()
        time.sleep(2)   


try:
    event = threading.Event()

    start_threard = threading.Thread(target=display, args=(event,))
    publish_threard = threading.Thread(target=publish, args=(event,))
    read_threard = threading.Thread(target=read, args=(event,))


    start_threard.start()
    publish_threard.start()
    read_threard.start()
    
    while True:
         time.sleep(1)
except KeyboardInterrupt:
    print("Ctrl+C pressed...")
    # event.set()  # inform the child thread that it should exit

    start_threard._Thread__stop()
    publish_threard._Thread__stop()
    read_threard._Thread__stop()



# while True:
#     connection_reset = connection_reset + 1
#     time.sleep(0.3)

# print(rabbit_url)
# #Loop in order to prevent connection failed
# while True:
#     SLEEP_TIME=0.1
#     conn = Connection(rabbit_url)
#     channel = conn.channel()
#     exchange = Exchange("exchange1", type="direct")
#     producer = Producer(exchange=exchange, channel=channel, routing_key="BOB")
#     queue = Queue(name="queue1", exchange=exchange, routing_key="BOB")
#     queue.maybe_bind(conn)
#     queue.declare()

#     producer.publish(time.time())

#     while True:


#         producer.publish("Hello there!")
#         print("PUBLISH")
#         time.sleep(SLEEP_TIME)
# media_exchange = Exchange('media', 'direct', durable=True)
# video_queue = Queue('video', exchange=media_exchange, routing_key='video')

# def process_media(body, message):
#     print body
#     message.ack()

# # connections
# with Connection('amqp://guest:guest@localhost//') as conn:

#     # produce
#     producer = conn.Producer(serializer='json')
#     producer.publish({'name': '/tmp/lolcat1.avi', 'size': 1301013},
#                       exchange=media_exchange, routing_key='video',
#                       declare=[video_queue])

#     # the declare above, makes sure the video queue is declared
#     # so that the messages can be delivered.
#     # It's a best practice in Kombu to have both publishers and
#     # consumers declare the queue. You can also declare the
#     # queue manually using:
#     #     video_queue(conn).declare()

#     # consume
#     with conn.Consumer(video_queue, callbacks=[process_media]) as consumer:
#         # Process messages and handle events on all channels
#         while True:
#             conn.drain_events()

# # Consume from several queues on the same channel:
# video_queue = Queue('video', exchange=media_exchange, key='video')
# image_queue = Queue('image', exchange=media_exchange, key='image')

# with connection.Consumer([video_queue, image_queue],
#                          callbacks=[process_media]) as consumer:
#     while True:
#         connection.drain_events()