# Terraform code for Rabbitmq on ECS

## Init repo

Need to get AWS credential and terraform installed.

```cd terraform```  
``` terraform init -backend-config="bucket=BUCKET_NAME"  -backend-config="key=STATE_PREFIX" -backend-config="region=$AWS_DEFAULT_REGION"```

* BUCKET_NAME is bucket name to store terraform state (need to have RW permission)
* STATE_PREFIX terraform will create folder into bucket with this name to store state


## Deploy

### Set variables
```
export TF_VAR_aws_region=$AWS_DEFAULT_REGION
export TF_VAR_az_count=3 # Number of AZ ino region
export TF_VAR_ssh_key_name # Name of ssh key
export TF_VAR_instance_type # EC2 instance type 
export TF_VAR_admin_cidr_ingress # IP range allow to access to ssh
export TF_VAR_vpc_cidr # VPC CIDR for example 10.11.0.0/16
```

### Check syntax
```terraform validate```

### See what will happen
```terraform plan```

### Start deployment
```terraform apply```

### Save outputs
```terraform output --json > ../code/env.json```


## Run code

### Build docker image run: 
```cd code```
```docker build -t rabbitmq_on_ecs . ```


### Start publisher
```docker run rabbitmq_on_ecs publisher```


### Start consumer
```docker run rabbitmq_on_ecs consumer```
