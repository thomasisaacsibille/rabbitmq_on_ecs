terraform {
  backend "s3" {}
}

data "aws_availability_zones" "available" {}


provider "aws" {
  region  = "${var.aws_region}"
}

provider "template" {}


module "rabbitmq" {
  source                            = "./rabbitmq"
  vpc_id                            = "${aws_vpc.main.id}"
  ssh_key_name                      = "${var.ssh_key_name}"
  subnet_ids                        = ["${aws_subnet.main.*.id}"]
  elb_additional_security_group_ids = ["${aws_security_group.lb_sg.id}"]
  nodes_additional_security_group_ids = ["${aws_security_group.instance_sg.id}"]
  min_size                          = "3"
  max_size                          = "3"
  desired_size                      = "3"
  instance_type                     = "t2.micro"
}


output "rabbitmq_elb_dns" {
  value = "${module.rabbitmq.rabbitmq_elb_dns}"
}

output "rabbit_password" {
  value     = "${module.rabbitmq.rabbit_password}"
}


