variable "aws_region" {
  description = "The AWS region to create things in."
}

variable "az_count" {
  description = "Number of AZs to cover in a given AWS region"
}

variable "ssh_key_name" {
  description = "Name of AWS key pair"
}

variable "instance_type" {
  default     = "t2.small"
  description = "AWS instance type"
}

variable "admin_cidr_ingress" {
  description = "CIDR to allow tcp/22 ingress to EC2 instance"
  default     = "0.0.0.0/0"
}

variable "vpc_cidr" {
  description = "CIDR for main VPC"
  default     = "10.11.0.0/16"
}
