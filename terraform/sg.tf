# Security groups

resource "aws_security_group" "instance_sg" {
  description = "controls direct access to application instances"
  vpc_id      = "${aws_vpc.main.id}"
  name        = "rabbitmq-ecs-instance-sg"

  ingress {
    protocol  = "tcp"
    from_port = 22
    to_port   = 22

    cidr_blocks = [
      "${var.admin_cidr_ingress}",
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}



resource "aws_security_group" "lb_sg" {
  description = "controls direct access to application instances"
  vpc_id      = "${aws_vpc.main.id}"
  name        = "lb-rabbitmq-ecs-instance-sg"

  # Allow port 80 for rabbitmq management web UI
  ingress {
    protocol  = "tcp"
    from_port = 80
    to_port   = 80

    cidr_blocks = [
      "0.0.0.0/0",
    ]
  }

  # Allow port 5672 for rabbitmq clients
  ingress {
    protocol  = "tcp"
    from_port = 5672
    to_port   = 5672

    cidr_blocks = [
      "0.0.0.0/0",
    ]
  }
}